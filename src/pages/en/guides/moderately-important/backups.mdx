---
layout: "@layouts/guides.astro"
title: "Protection: Backups"
topic: "How to Keep Good Backups"
weight: 5
draft: false
---

import ProsAndCons from "@components/charts/ProsAndCons.astro";
import data from "@assets/data/pages/en/guides/moderately-important/Backups.json";

# Protection: Backups

**Backups are critical since devices regularly fail, break, or get stolen, lost, or corrupted.** To develop good backup habits, **first you need to decide how much space you need.** If you're only worried about backing up important text files and financial documents, you probably don't need more than a few gigabytes. If you'll be backing up videos and images, you'll want something more in the hundreds of gigabytes or few terabytes range.

**Next, you'll need to decide how often you need to back up and how far back you need to keep your backups.** This will play a part in deciding your storage size. Even if your one-time backup is small, keeping weekly copies can add up quickly. Decide if you want to keep a specific amount of backups (ex: six-month's worth of weekly backups) or just the most recent however-many it can hold (or less), with the oldest ones being deleted to make space for the newest ones.

**Finally, come up with a system.** Windows and Mac have features that allow you to automate the backup process including frequency, which files to include, and where to store them. These are fine systems to put in place, just remember to make sure your encrypted storage location is unlocked if encrypted so the backup is able to take place. If you decide to manually handle your backups, be sure to set regular reminders so you don't forget. Automatic backups are desirable when possible.

## The 3-2-1 Rule

The 3-2-1 Rule is a guideline for considering how to organize your backups effectively. It states that you should have 3 copies of your data - 2 backups plus your live (daily in-use) copy. You should have 2 separate formats for your backups - such as an external hard drive and a cloud copy. Finally, you should have 1 of those copies offsite - again, a cloud copy or a USB at a friend's house - in case of physical damage or disaster at your location.

## Privacy-Respecting Cloud Backups

Generally speaking, **I advise against using Google Drive, Dropbox, iCloud, or similar services.** One reason is because they are not [zero-knowledge](/guides/moderately-important/encryption). Even using them with one of the solutions in the next section, they can see that you have encrypted files in your storage space, and we don't know if someday they'll decide to take an anti-encryption stance and delete it or your account. There is always an anti-encryption rhetoric coming from numerous governments around the world. Finally, [Google Drive](https://cloud.google.com/security/encryption-at-rest/default-encryption/) and [Apple iCloud](https://support.apple.com/en-us/HT202303) use weak encryption standards in some cases.

<img src="/images/logos/nextcloud.png" alt="Nextcloud logo" class="float-right mx-6 w-24" />If your backup solution is a local hard drive, I discussed using Veracrypt in the previous [section](/guides/moderately-important/devices) to encrypt your device. **There are several secure and private cloud backup solutions.** The best-case scenario is to self-host a [Nextcloud](https://nextcloud.com/) server so you have complete and total control of the data on a trusted, open source platform. However, this can be unrealistic to many for a number of reasons, so one option is to select a [provider](https://nextcloud.com/signup/). (Please note that Nextcloud essentially [cannot](https://nextcloud.com/blog/encryption-in-nextcloud/) be zero-knowledge at this time. Make sure you trust the provider, or use Cryptomater (discussed below) for added protection.) If this option doesn't meet your needs, I have selected several suggestions below.

_Listed in alphabetical order, not order of recommendation_

<ProsAndCons data={data} headingLevel={4} />

**[Click here to see my criteria for selecting these services](https://gitlab.com/thenewoil/website/-/wikis/Backup-&-Cloud-Storage-Criteria)**

_[Click here for a visual version of this chart](/charts/backups)_

### Mainstream Cloud Providers

<img src="/images/logos/cryptomater.png" alt="Cryptomater logo" class="float-left mx-6 w-24" />**If none of these options work for you, there are two ways to upload encrypted content to mainstream cloud providers** like Google Drive or iCloud. The first is [Cryptomater](https://cryptomator.org/), an open source tool that allows you to encrypt each individual file and sync it with the cloud. It works for Google Drive and Dropbox. If you aren't using one of those services or otherwise don't want to use Cryptomater, then consider the following strategy:

First, figure out how much storage you need. Google Drive offers 15 gigabytes for free, Apple iCloud offers 5 gigabytes for free, and Dropbox offers 2 gigabytes for free. Next, make sure you have installed that service's file sync application. This is typically an app that will create a folder on your computer, and that folder acts as a real-time sync between your account and your computer. It's designed to make working directly from the file in your account effortless.

Now open up Veracrypt, select the "Tools" menu, and choose "Volume Creation Wizard." Pick "[Create](*PUBLIC*/images/steps/vera-crypt-encrypting/c01.jpg) an encrypted file container," "[Standard](*PUBLIC*/images/steps/vera-crypt-external-encrypting/b02.jpg) Veracrypt Volume," then click "Select File" and [navigate](*PUBLIC*/images/steps/vera-crypt-encrypting/c03.jpg) to your cloud service folder. Once in the folder, you'll have to makeup a nonexistant file name. Anything works, from "Backup" to "veracrypt_containter" or whatever you want. Once you hit "save," it should show you the file path. Continue onward, making sure you've selected "[AES](*PUBLIC*/images/steps/vera-crypt-encrypting/c04.jpg)" and "SHA-512" for your algorithms, and then move on. The next screen will ask you for a [volume size](*PUBLIC*/images/steps/vera-crypt-encrypting/c05.jpg). Ideally, I would say use as much as you can. If you use your Dropbox or Google Drive for other sharing purposes, leave enough space free for that or maybe only use the exact amount of space you require for your backup strategy. Once you decide what storage size is appropriate for you, go to the next screen where it requires a password. From there, it's pretty self explanatory. Just answer the questions and it will pick the best formats and such for you.

**If you follow these steps, you should have created secure, consistent backups** that will protect you in the event of a lost, stolen, or damaged device, or even the dreaded ransomware.
